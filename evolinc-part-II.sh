#!/bin/bash
#author: Andrew Nelson; andrew.d.l.nelson@gmail.com
# Script to perform comparative genomic analysis of lincRNAs
# Usage: 
# sh evolinc-part-II.sh -b Blasting_list 

usage() {
      echo ""
      echo "Usage : sh $0 -b BLASTing_list -l Query_lincRNA -q Query_species"
      echo ""

cat <<'EOF'
  -b </path/to/BLASTing_list in tab-delimited format>

  -l </path/to/query lincRNA>

  -q <query species>

  -h Show this usage information

EOF
    exit 0
}

while getopts ":hb:q:l:" opt; do
  case $opt in
    b)
     Blasting_list=$OPTARG #This is a five-column tab-delimited list in the following order:
		# subject_genome lincRNA_fasta	query_species (four letter Genus-species designation ie., Gspe)	subject_species (same four letter abbreviation as subject_species)	               subject_gff (in fasta_format) # All of these files should be in the current working folder
     ;;
    l)
    query_lincRNA=$OPTARG # Query lincRNA file here
     ;;
    q)
     query_species=$OPTARG # This is entirely left to the user
     ;;
    h)
     usage  
     exit 1
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

# Make all necessary folders
mkdir -p BLAST_DB
mkdir -p Homology_Search
mkdir -p Reciprocal_BLAST
mkdir -p Orthologs

# Initiate search for putative orthologs
echo "***Starting lincRNA to Genome Comparisons***"
python startup_script.py $Blasting_list
echo "***Finished with lincRNA to Genome Comparisons***"

#Create a list of all genomes, lincRNA ortholog, and gff files to set up reciprocal BLAST
#I should think of a way to hide this in a separate script
cd Reciprocal_BLAST
find . -maxdepth 1 -name "*genome*" >Reciprocal_chrom_list.txt
find . -maxdepth 1 -name "*orthologs*" >Reciprocal_lincRNAs_list.txt
find . -maxdepth 1 -name "*coords*" >Reciprocal_lincRNAs_coord_list.txt
sed -i 's~./~~g' Reciprocal_chrom_list.txt
sed -i 's~./~~g' Reciprocal_lincRNAs_list.txt
sed -i 's~./~~g' Reciprocal_lincRNAs_coord_list.txt
sort -n Reciprocal_chrom_list.txt -o Reciprocal_chrom_list.txt
sort -n Reciprocal_lincRNAs_list.txt -o Reciprocal_lincRNAs_list.txt
sort -n Reciprocal_lincRNAs_coord_list.txt -o Reciprocal_lincRNAs_coord_list.txt
:|paste Reciprocal_chrom_list.txt - Reciprocal_lincRNAs_list.txt - Reciprocal_lincRNAs_coord_list.txt >Reciprocal_list.txt
sed -i 's~\t\t~\t~g' Reciprocal_list.txt
sed -i "s/$/\t$query_species.$query_species.coords.gff/" Reciprocal_list.txt
sed -i "s/$/\t$query_species/" Reciprocal_list.txt
sed -i "s/$/\t$query_species.genome.fasta/" Reciprocal_list.txt
#sed -i "s~\.putative~_putative~g" Reciprocal_list.txt
sed -i "s~\.genome~_genome~g" Reciprocal_list.txt
rm Reciprocal_chrom_list.txt
rm Reciprocal_lincRNAs_list.txt
rm Reciprocal_lincRNAs_coord_list.txt
echo "***Starting Reciprocal Search***"

# Confirm the reciprocity of the putative orthologs
python ../Reciprocal_BLAST_startup_script.py Reciprocal_list.txt
echo "***Finished with Reciprocal Search***"

#Starting building families
echo "***Creating Families of similar sequences***"
cd ../Orthologs
cat * > All_orthologs.fasta

grep ">" /evolinc-ii/$query_lincRNA | sed 's/>//' > /evolinc-ii/$query_lincRNA.list

perl ../find_from_list.pl /evolinc-ii/$query_lincRNA.list All_orthologs.fasta

cd lincRNA_families

for i in *FASTA; do mv $i "`basename $i _.FASTA`.fasta"; done
echo "Finished Creating Families of similar sequences"
echo "Starting alignments"
ls * > alignment_list.txt
sed -i '/alignment_list.txt/d' alignment_list.txt
mkdir -p Final_results

perl ../../Batch_MAFFT.pl alignment_list.txt

cd Final_results

grep -c ">" * | grep -v ':0$' | grep -v ':1$' | grep -v ':2$' | grep -v ':3$' | awk -F':' '{print $1}' >aligned_list.txt

echo "Finished alignments"

#ls * >aligned_list.txt
#sed -i '/aligned_list.txt/d' aligned_list.txt
#mkdir -p ../RAxML_families
#perl ../../../Batch_Raxml.pl aligned_list.txt

#rm aligned_list.txt

#python ../../../Family_division_and_summary.py ../../../species_list.txt

# Need to insert a set of scripts to insert additional structural data (such as from pip-seq).
# NOTUNG variables
#      setenv CLASSPATH $CLASSPATH:<pathname> 
#ls -A *.newick >batch_list_for_NOTUNG.txt
#sed -i 1i"$Species_tree" batch_list_for_NOTUNG.txt
#java -jar Notung-2.6.jar -b batch_list_for_NOTUNG.txt --root --treeoutput newick --nolosses --speciestag prefix --savepng --treestats --info --log --homologtabletabs --edgeweights name
#java -jar Notung-2.6.jar -b batch_list_for_NOTUNG.txt --rearrange --threshold 70 --treeoutput newick --nolosses --speciestag prefix --savepng --treestats --info --log --homologtabletabs --edgeweights name  --stpruned
